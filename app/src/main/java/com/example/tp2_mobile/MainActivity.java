package com.example.tp2_mobile;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    //ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
    private WineDbHelper wineHelp;
    private SimpleCursorAdapter sca;
    Intent myIntent;
    Cursor result;
    ListView wine;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        wineHelp = new WineDbHelper(this);

        UpdateCursor();

        wine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                result.moveToPosition(position);

                Wine vin = WineDbHelper.cursorToWine(result);

                myIntent = new Intent(MainActivity.this, WineActivity.class).putExtra("Wine", vin);

                startActivity(myIntent);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Wine vin = new Wine("Nouveau Vin","","","","");
                wineHelp.addWine(vin);

                //myIntent = new Intent(MainActivity.this, WineActivity.class).putExtra("Wine", vin);

                //startActivity(myIntent);

                Snackbar.make(view, "Un nouveau vin à été créé.", Snackbar.LENGTH_LONG)
                       .setAction("Action", null).show();

               UpdateCursor();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void UpdateCursor(){
        result=wineHelp.fetchAllWines();

        String[] columns = new String[] {
                WineDbHelper.COLUMN_NAME,
                WineDbHelper.COLUMN_WINE_REGION
        };

        int[] to = new int[]{
                R.id.ligne_a,
                R.id.ligne_b
        };

        sca = new SimpleCursorAdapter(this, R.layout.twolignes, result, columns, to, 0);

        wine=(ListView)findViewById(R.id.listWine);

        wine.setAdapter(sca);

    }

    @Override
    public void onResume(){
        super.onResume();

        UpdateCursor();

    }
}

